from ipapp import BaseApplication, BaseConfig, main
from ipapp.asgi.uvicorn import Uvicorn, UvicornConfig

from fastapi.applications import FastAPI

import logging
from sys import argv


fapp = FastAPI()


@fapp.get('/')
async def index():
    return dict(
        path='/',
        message='index',
    )


class Config(BaseConfig):
    asgi: UvicornConfig


class App(BaseApplication):
    def __init__(self, cfg: Config) -> None:
        super().__init__(cfg)

        self.add('srv', Uvicorn(cfg.asgi, fapp))


if __name__ == '__main__':
    """
    """
    logging.basicConfig(level=logging.INFO)
    main(argv, '0.0.1', App, Config)
